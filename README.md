# Git Works

## Git Flow

Install these two packages on top of your git and git completions packages:
- https://github.com/petervanderdoes/gitflow-avh
- https://github.com/petervanderdoes/git-flow-completion
